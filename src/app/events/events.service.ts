import {EventEmitter, Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {IEvent, ISession} from './event.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {HttpUtils} from '../common/http-utils';

@Injectable()
export class EventService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getEvents(): Observable<IEvent[]> {
    return this.http.get<IEvent[]>('/api/events').pipe(catchError(HttpUtils.handleError<IEvent[]>('getEvents', [])));
  }

  saveEvent(event: IEvent) {
    return this.http.post<IEvent>('/api/events', event, {headers: new HttpHeaders({'content-type': 'application/json'})})
      .pipe(catchError(HttpUtils.handleError<IEvent>('saveEvent')));
  }

  getEventById(id: number): Observable<IEvent> {
    return this.http.get<IEvent>('/api/events/' + id).pipe(catchError(HttpUtils.handleError<IEvent>('getEvent')));
  }

  searchSessions(searchTerm): Observable<ISession[]> {
    return this.http.get<ISession[]>('/api/sessions/search?search=' + searchTerm)
      .pipe(catchError(HttpUtils.handleError<ISession[]>('searchSessions')));
  }
}
