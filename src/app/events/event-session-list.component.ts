import {Component, Input, OnChanges} from '@angular/core';
import {ISession} from './event.model';
import {assembleBoundTextPlaceholders} from '@angular/compiler/src/render3/view/i18n/util';
import {AuthService} from '../user/auth.service';
import {VoterService} from './voter.service';

@Component({
  selector: 'event-session-list',
  templateUrl: './event-session-list.component.html'
})
export class EventSessionListComponent implements OnChanges {
  @Input() eventId: number;
  @Input() sessions: ISession[];
  @Input() filterBy: string;
  @Input() sortBy: string;

  visibleSessions: ISession[] = [];

  private authService: AuthService;
  private voterService: VoterService;

  constructor(authService: AuthService, voterService: VoterService) {
    this.authService = authService;
    this.voterService = voterService;
  }

  ngOnChanges() {
    if (this.sessions) {
      this.filterSessions(this.filterBy);
      this.sortBy === 'name' ? this.visibleSessions.sort(nameComparator(true)) : this.visibleSessions.sort(voterComparator(false));
    }
  }

  toggleVote(session: ISession) {
    if (this.userHasVoted(session)) {
      this.voterService.deleteVoter(this.eventId, session, this.authService.currentUser.userName);
    } else {
      this.voterService.addVoter(this.eventId, session, this.authService.currentUser.userName);
    }
    if (this.sortBy === 'votes') {
      this.visibleSessions.sort(voterComparator(false));
    }
  }

  userHasVoted(session: ISession) {
    return this.voterService.userHasVoted(session, this.authService.currentUser.userName);
  }

  filterSessions(filter) {
    if (filter === 'all') {
      this.visibleSessions = this.sessions.slice(0);
    } else {
      this.visibleSessions = this.sessions.filter(sessions => {
        return sessions.level.toLocaleLowerCase() === filter;
      });
    }
  }
}

function nameComparator(ascending: boolean) {
  const direction = ascending ? 1 : -1;

  return (s1: ISession, s2: ISession) => {
    let result: number;

    if (s1.name > s2.name) {
      result = 1;
    } else if (s1.name === s2.name) {
      result = 0;
    } else {
      result = -1;
    }

    return result * direction;
  };
}

function voterComparator(ascending: boolean) {
  const direction = ascending ? 1 : -1;

  return (s1: ISession, s2: ISession) => {
    return (s1.voters.length - s2.voters.length) * direction;
  };
}
