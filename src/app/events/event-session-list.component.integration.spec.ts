import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {EventSessionListComponent} from './event-session-list.component';
import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {AuthService} from '../user/auth.service';
import {VoterService} from './voter.service';
import {ISession} from './event.model';
import {UpvoteComponent} from './upvote.component';
import {DurationPipe} from './duration-pipe';
import {CollapsibleWellComponent} from '../common/collapsible-well.component';
import {By} from '@angular/platform-browser';

describe('EventSessionListComponent', () => {
  let fixture: ComponentFixture<EventSessionListComponent>,
    component: EventSessionListComponent,
    element: HTMLElement,
    debugEl: DebugElement;

  beforeEach(async(() => {
    const mockAuthService = {};
    const mockVoterService = {};

    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        EventSessionListComponent,
        // UpvoteComponent,
        DurationPipe,
        // CollapsibleWellComponent
      ],
      providers: [
        {provide: AuthService, useValue: mockAuthService},
        {provide: VoterService, useValue: mockVoterService}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(EventSessionListComponent);
      component = fixture.componentInstance;
      debugEl = fixture.debugElement;
      element = fixture.nativeElement;
    });

    describe('initial display', () => {
      it('should have the correct session title', () => {
        component.sessions = <ISession[]>[
          {id: 3, name: 'session 1', presenter: 'Joe', duration: 2, level: 'intermediate', abstract: 'abstract', voters: ['Joe', 'John']}];
        component.filterBy = 'all';
        component.sortBy = 'name';
        component.eventId = 3;

        component.ngOnChanges();
        fixture.autoDetectChanges();

        expect(element.querySelector('[well-title]').textContent).toContain('session 1');
        expect(debugEl.query(By.css('[well-title')).nativeElement.textContent).toContain('session 1');
      });
    });
  }));
});
