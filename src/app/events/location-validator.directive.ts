import {Directive} from '@angular/core';
import {AbstractControl, FormGroup, NG_VALIDATORS, Validator} from '@angular/forms';

@Directive({
  selector: '[validateLocation]',
  providers: [{provide: NG_VALIDATORS, useExisting: LocationValidatorDirective, multi: true}]
})
export class LocationValidatorDirective implements Validator {
  validate(formGroup: FormGroup): {[key: string]: any} {
    if ((this.populated(formGroup.controls['address'])
      && this.populated(formGroup.controls['city'])
      && this.populated(formGroup.controls['country']))
      || this.populated((<FormGroup> formGroup.root).controls['onlineUrl'])) {
      return null;
    } else {
      return {validateLocation: false};
    }
  }

  populated(formControl: AbstractControl): boolean {
    return formControl && formControl.value;
  }
}
