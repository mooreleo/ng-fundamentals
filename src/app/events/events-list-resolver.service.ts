import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {map} from 'rxjs/operators';
import {EventService} from './events.service';

@Injectable()
export class EventsListResolver implements Resolve<any> {
  eventService;

  constructor(eventsService: EventService) {
    this.eventService = eventsService;
  }

  resolve() {
    return this.eventService.getEvents();
  }
}
