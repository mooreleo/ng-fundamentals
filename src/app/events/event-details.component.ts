import {Component, OnInit} from '@angular/core';
import {EventService} from './events.service';
import {ActivatedRoute, Params} from '@angular/router';
import {IEvent, ISession} from './event.model';

@Component({
  templateUrl: './event-details.component.html',
  styles: [`
    h3 {display:table-cell; vertical-align: center}
    .container {padding-left: 20px; padding-right: 20px;}
    .event-image {height: 100px;}
    .btn {margin-bottom: 10px; margin-right: 5px; margin-left: 5px;}
    .btn-group {margin-left: 10px; margin-right: 10px;}
    a {cursor:pointer}
  `]
})
export class EventDetailsComponent implements OnInit {
  eventService: EventService;
  event: IEvent;
  route: ActivatedRoute;
  addMode = false;
  filterBy = 'all';
  sortBy = 'voters';

  constructor(eventService: EventService, route: ActivatedRoute) {
    this.eventService = eventService;
    this.route = route;
  }

  ngOnInit() {
    this.route.data.forEach((data) => {
        this.event = this.route.snapshot.data['event'];
        this.addMode = false;
    });
  }

  addSession() {
    this.addMode = true;
  }

  saveNewSession(session: ISession) {
    const nextId = Math.max.apply(null, this.event.sessions.map(s => s.id));
    session.id = nextId + 1;
    this.event.sessions.push(session);
    this.eventService.saveEvent(this.event).subscribe();
    this.addMode = false;
  }

  cancelAddSession() {
    this.addMode = false;
  }
}
