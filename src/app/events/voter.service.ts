import {Injectable} from '@angular/core';
import {IEvent, ISession} from './event.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {HttpUtils} from '../common/http-utils';

@Injectable()
export class VoterService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  deleteVoter(eventId: number, session: ISession, voterName: string) {
    session.voters = session.voters.filter(voter => voter !== voterName);

    this.http.delete(`/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`)
      .pipe(catchError(HttpUtils.handleError<IEvent>('deleteVoter')))
      .subscribe();
  }

  addVoter(eventId: number, session: ISession, voterName: string) {
    session.voters.push(voterName);

    return this.http.post<IEvent>(`/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`,
      {}, {headers: new HttpHeaders({'content-type': 'application/json'})})
      .pipe(catchError(HttpUtils.handleError<IEvent>('addVoter')))
      .subscribe();
  }

  userHasVoted(session: ISession, voterName: string) {
    return session.voters.some(voter => voter === voterName);
  }
}
