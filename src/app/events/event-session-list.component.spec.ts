import {EventSessionListComponent} from './event-session-list.component';
import {ISession} from './event.model';

describe('EventSessionListComponent', () => {
  let component: EventSessionListComponent;
  let mockAuthService, mockVoterService;

  beforeEach(() => {
    component = new EventSessionListComponent(mockAuthService, mockVoterService);
  });

  describe('ngOnChanges', () => {
    it('should filter the session correctly', () => {
      component.sessions = <ISession[]>[
        {name: 'session 1', level: 'intermediate'},
        {name: 'session 4', level: 'beginner'},
        {name: 'session 2', level: 'intermediate'}];
      component.filterBy = 'intermediate';
      component.sortBy = 'name';
      component.eventId = 3;

      component.ngOnChanges();

      expect(component.visibleSessions.length).toBe(2);
    });
  });

  describe('ngOnChanges', () => {
    it('should sort the session correctly', () => {
      component.sessions = <ISession[]>[
        {name: 'session 1', level: 'intermediate'},
        {name: 'session 4', level: 'beginner'},
        {name: 'session 2', level: 'intermediate'}];
      component.filterBy = 'all';
      component.sortBy = 'name';
      component.eventId = 3;

      component.ngOnChanges();

      expect(component.visibleSessions.length).toBe(3);
      expect(component.visibleSessions[0].name).toBe('session 1');
      expect(component.visibleSessions[1].name).toBe('session 2');
      expect(component.visibleSessions[2].name).toBe('session 4');
    });
  });
});
