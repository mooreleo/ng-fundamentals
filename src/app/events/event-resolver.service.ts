import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {map} from 'rxjs/operators';
import {EventService} from './events.service';

@Injectable()
export class EventsResolver implements Resolve<any> {
  private eventService;

  constructor(eventsService: EventService) {
    this.eventService = eventsService;
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.eventService.getEventById(route.params['id']);
  }
}
