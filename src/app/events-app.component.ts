import {Component, OnInit} from '@angular/core';
import {AuthService} from './user/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'events-app',
  template: `
    <nav-bar></nav-bar>
    <router-outlet></router-outlet>
  `
})
export class EventsAppComponent implements OnInit{
  private authService;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  ngOnInit() {
    this.authService.checkAutheticationStatus();
  }
}
