import {Component} from '@angular/core';
import {AuthService} from '../user/auth.service';
import {ISession} from '../events/event.model';
import {EventService} from '../events/events.service';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: [`
    .nav.navbar-nav {font-size: 15px;}
    #searchForm {margin-right: 100px;}
    @media (max-width: 1200px) {#searchForm {display: none;}}
    li > a.active {color: #F97924;}
  `]
})
export class NavBarComponent {
  eventService: EventService;
  authService: AuthService;
  searchTerm = '';
  foundSessions: ISession[];

  constructor(eventService: EventService, authService: AuthService) {
    this.eventService = eventService;
    this.authService = authService;
  }

  searchSessions(searchTerm) {
    this.eventService.searchSessions(searchTerm).subscribe(sessions => this.foundSessions = sessions);
  }
}
