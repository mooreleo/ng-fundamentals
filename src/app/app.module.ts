import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {EventsAppComponent} from './events-app.component';
import {EventsListComponent} from './events/events-list.component';
import {EventThumbnailComponent} from './events/event-thumbnail.component';
import {NavBarComponent} from './nav/nav-bar.component';
import {EventService} from './events/events.service';
import {Toastr, TOASTR_TOKEN} from './common/toastr.service';
import {EventDetailsComponent} from './events/event-details.component';
import {PreloadAllModules, RouterModule} from '@angular/router';
import {appRoutes} from '../../routes';
import {CreateEventComponent} from './events/create-event.component';
import {Error404Component} from './errors/404.component';
import {EventsListResolver} from './events/events-list-resolver.service';
import {AuthService} from './user/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CreateEventSessionComponent} from './events/create-event-session.component';
import {EventSessionListComponent} from './events/event-session-list.component';
import {CollapsibleWellComponent} from './common/collapsible-well.component';
import {DurationPipe} from './events/duration-pipe';
import {JQUERY_TOKEN} from './common/jQuery.service';
import {SimpleModalComponent} from './common/simple-modal.component';
import {ModalTriggerDirective} from './common/modal-trigger.directive';
import {UpvoteComponent} from './events/upvote.component';
import {VoterService} from './events/voter.service';
import {LocationValidatorDirective} from './events/location-validator.directive';
import {HttpClientModule} from '@angular/common/http';
import {EventsResolver} from './events/event-resolver.service';

const toastr: Toastr = window['toastr'];
const jQuery: Toastr = window['$'];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules}),
    HttpClientModule
  ],
  declarations: [
    NavBarComponent,
    Error404Component,
    EventsAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    EventDetailsComponent,
    CreateEventComponent,
    CreateEventSessionComponent,
    EventSessionListComponent,
    CollapsibleWellComponent,
    DurationPipe,
    SimpleModalComponent,
    ModalTriggerDirective,
    UpvoteComponent,
    LocationValidatorDirective
  ],
  providers: [
    EventService,
    {
      provide: TOASTR_TOKEN,
      useValue: toastr
    },
    {
      provide: JQUERY_TOKEN,
      useValue: jQuery
    },
    EventsListResolver,
    EventsResolver,
    AuthService,
    VoterService,
    {
      provide: 'canDeactivateCreateEvent',
      useValue: checkDirtyState
    }
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }

export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('You have not saved this event, do you really want to cancel?');
  }
  return true;
}
