import {Injectable} from '@angular/core';
import {IUser} from './user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {IEvent} from '../events/event.model';
import {HttpUtils} from '../common/http-utils';

@Injectable()
export class AuthService {
  private http: HttpClient;

  currentUser: IUser;

  constructor(http: HttpClient) {
    this.http = http;
  }

  loginUser(userName: string, password: string) {
    // this.currentUser = {
    //   id: 1,
    //   userName: userName,
    //   firstName: 'John',
    //   lastName: 'Papa'
    // };
    return this.http.post('/api/login', {username: userName, password: password},
      {headers: new HttpHeaders({'content-type': 'application/json'})})
      .pipe(tap(data => {
        this.currentUser = <IUser>data['user'];
      }))
      .pipe(catchError(err => {
          return of(false);
      }));
  }

  logoutUser() {
    this.currentUser = undefined;

    return this.http.post('/api/logout', {}, {headers: new HttpHeaders({'content-type': 'application/json'})});
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  checkAutheticationStatus() {
    this.http.get('/api/currentIdentity').pipe(tap(data => {
      if (data instanceof Object) {
        this.currentUser = <IUser>data;
      } else {
        this.currentUser = undefined;
      }
    })).subscribe();
  }

  updateCurrentUser(firstName: string, lastName: string) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;

    return this.http.put(`/api/users/${this.currentUser.id}`, this.currentUser,
      {headers: new HttpHeaders({'content-type': 'application/json'})})
      .pipe(catchError(HttpUtils.handleError<IEvent>('updateCurrentUser')));
  }
}
