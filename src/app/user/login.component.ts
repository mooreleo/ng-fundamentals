import {Component} from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {
  router;
  authService;
  userName;
  password;
  loginInvalid = false;

  constructor(authService: AuthService, router: Router) {
    this.authService = authService;
    this.router = router;
  }

  login(formValues) {
    this.authService.loginUser(formValues.userName, formValues.password)
      .subscribe((resp => {
        if (resp === false) {
          this.loginInvalid = true;
        } else {
          this.router.navigate(['events']);
        }
      }));
  }

  cancel() {
    this.router.navigate(['events']);
  }
}
