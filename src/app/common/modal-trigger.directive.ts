import {Directive, ElementRef, Inject, Input, OnInit} from '@angular/core';
import {JQUERY_TOKEN} from './jQuery.service';

@Directive({
  selector: '[modal-trigger]'
})
export class ModalTriggerDirective implements OnInit {
  @Input('modal-trigger') modalId: string;

  private $: any;
  private el: HTMLElement;

  constructor(elementRef: ElementRef, @Inject(JQUERY_TOKEN) $: any) {
    this.$ = $;
    this.el = elementRef.nativeElement;
  }

  ngOnInit() {
    this.el.addEventListener('click', e => {
      this.$(`#${this.modalId}`).modal({});
    });
  }
}
