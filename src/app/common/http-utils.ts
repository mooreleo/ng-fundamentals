import {Observable, of} from 'rxjs';

// Done right by Leo Moore!
export const HttpUtils = Object.freeze({
  handleError: function<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
});
