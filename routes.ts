import {EventsListComponent} from './src/app/events/events-list.component';
import {EventDetailsComponent} from './src/app/events/event-details.component';
import {Routes} from '@angular/router';
import {CreateEventComponent} from './src/app/events/create-event.component';
import {Error404Component} from './src/app/errors/404.component';
import {EventsListResolver} from './src/app/events/events-list-resolver.service';
import {CreateEventSessionComponent} from './src/app/events/create-event-session.component';
import {EventsResolver} from './src/app/events/event-resolver.service';

export const appRoutes: Routes = [
  {path: '', redirectTo: '/events', pathMatch: 'full' },
  {path: '404', component: Error404Component},
  {path: 'events', component: EventsListComponent, resolve: {events: EventsListResolver}},
  {path: 'events/session/new', component: CreateEventSessionComponent},
  {path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent']},
  {path: 'events/:id', component: EventDetailsComponent, resolve: {event: EventsResolver}},
  {path: 'user', loadChildren: './user/user.module#UserModule'}
];
